;;(in-package :log4cl)
;;(log:config :off)
(in-package :nyxt-user)

; theme
(nyxt::load-lisp "~/.config/nyxt/themes/dracula/init.lisp")

;;auto-mode config
;(define-configuration nyxt/auto-mode:auto-mode
;		        ((nyxt/auto-mode:prompt-on-mode-toggle t)))

; enable auto-mode config (only works with blocker-mode - bug?)
(define-configuration (web-buffer nosave-buffer)
		      ((default-modes (append %slot-default%
			      '(reduce-tracking-mode)
			      '(blocker-mode)
			      '(auto-mode)
			      '(vi-normal-mode)))))

