(in-package #:nyxt-user)

;; Import Files
(dolist (file (list (nyxt-init-file "themes/dracula/statusline.lisp")
                    (nyxt-init-file "themes/dracula/stylesheet.lisp")))
  (load file))
