-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- Temporary workaround for wayland/hpyrland issue
--if os.getenv('XDG_CURRENT_DESKTOP') == 'Hyprland' then
--    config.enable_wayland = false
--end

function scheme_for_appearance(appearance)
  if appearance:find 'Dark' then
    --return 'Gruvbox dark, hard'
    --return 'Unikitty Dark'
    --return 'OneDark'
    --return 'Darcula'
    --return 'Atelier Cave'
    return 'Atelier Lakeside'
    --return 'catppuccin-macchiato'
  else
    --return 'Gruvbox light, hard'
    --return 'Unikitty Light'
    --return 'OneLight'
    --return 'Atelier Cave Light'
    return 'Atelier Lakeside Light'
  end
end

config.color_scheme = scheme_for_appearance(wezterm.gui.get_appearance())
config.window_background_opacity = 0.85

config.enable_tab_bar = false

-- set default shell to bash shell without login mode
config.default_prog = {"/bin/bash"}

-- define key bindings
config.keys = {
  { key = 'l', mods = 'ALT', action = wezterm.action.ShowLauncher },
  -- disable key bindings that interfere with other terminal applications
  { key = 'F', mods = 'CTRL', action = 'DisableDefaultAssignment' },
  { key = 't', mods = 'SUPER', action = 'DisableDefaultAssignment' },
  { key = 't', mods = 'META', action = 'DisableDefaultAssignment' },
  { key = 't', mods = 'WIN', action = 'DisableDefaultAssignment' },
  { key = 't', mods = 'CMD', action = 'DisableDefaultAssignment' },
  { key = 'T', mods = 'CTRL', action = 'DisableDefaultAssignment' },
  { key = 't', mods = 'CTRL', action = 'DisableDefaultAssignment' },
--  { key = 'V', mods = 'CTRL', action = 'DisableDefaultAssignment' },
--  { key = 'v', mods = 'CTRL', action = 'DisableDefaultAssignment' },
--  { key = 'X', mods = 'CTRL', action = 'DisableDefaultAssignment' },
--  { key = 'x', mods = 'CTRL', action = 'DisableDefaultAssignment' },
}

-- and finally, return the configuration to wezterm
return config

