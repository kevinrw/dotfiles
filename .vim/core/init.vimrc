""""""""""""""""""""""""""""""""""""""""""""""""
" Core Init Vimrc
""""""""""""""""""""""""""""""""""""""""""""""""
" this is a standalone minimal vimrc
" ideally no plugin execution should go here
" non-ideally they can be wrapped in a try-catch

""""""""""""""""""""""""""""""""""""""""""""""""
" General Configuration
""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/core/config.vimrc

""""""""""""""""""""""""""""""""""""""""""""""""
" Functions
""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/core/functions.vimrc

""""""""""""""""""""""""""""""""""""""""""""""""
" Mappings
""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/core/mappings.vimrc

""""""""""""""""""""""""""""""""""""""""""""""""
" Automations
""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/core/automations.vimrc

