""""""""""""""""""""""""""""""""""""""""""""""""
" General Configuration
""""""""""""""""""""""""""""""""""""""""""""""""
set number
" If you are using a ISO-8613-3 compatible terminal ( vim docs, neovim docs), and you see a green or blue line, try to enable termguicolors
set termguicolors

set splitright " works best with Lexplore

" Height of the command bar
set cmdheight=2

set hlsearch	    " highlight search matches
set incsearch	    " search while characters are entered

" smart search case-insensitive only when all lower-case
set ignorecase
set smartcase

set showcmd	" show last command in bottom right
set ruler	" always show current position
set showmatch	" show matching braces
"set wildmenu 	" visual autocomplete for command menu
set wildmenu wildmode=list:full
set wildcharm=<C-z>
set wildignore+=*~ wildignorecase
set noautochdir

set switchbuf=useopen

" enable mouse support in normal mode
set mouse=n

""""""""""""""""""""""""""""""""""""""""""""""""
" Explorer configuration
""""""""""""""""""""""""""""""""""""""""""""""""
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 0
let g:netrw_altv = 1
let g:netrw_winsize = 25
"let g:netrw_keepdir = 0 " track browsing directory
let g:netrw_keepdir = 1 " cd on directory to change browsing directory
let g:netrw_usetab = 1 " enable the <tab> map supporting shrinking/expanding Lexplore

" open explorer on startup by default
"augroup ProjectDrawer
"  autocmd!
"  autocmd VimEnter * :Lexplore
"augroup END

"""""""""""""""""""""""""""""""""""""""""""""""""
" Backups, Swap Files
"""""""""""""""""""""""""""""""""""""""""""""""""
set nobackup
set nowb
set noswapfile

"""""""""""""""""""""""""""""""""""""""""""""""""
" Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax on

" UTF-8 encoding and en_US as default encoding/language
set encoding=utf8

" Define standard filetype
set ffs=unix,dos,mac

if has('gui_running')
    set background=dark
else
    let base16colorspace=256  " Access colors present in 256 colorspace
endif

set cursorline	" highlight current active line

" initial/default colorscheme to start with (may be overriden by plugins)
" ideally should be one that is included on system by default
try
    colorscheme slate
catch
    echo "failed to load colorscheme"
endtry

"""""""""""""""""""""""""""""""""""""""""""""""""
" File Types
"""""""""""""""""""""""""""""""""""""""""""""""""
" recognize .md files as markdown files
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown

" enable spell-checking for markdown files
autocmd BufRead,BufNewFile *.md setlocal spell

"""""""""""""""""""""""""""""""""""""""""""""""""
" Text and Indentation
"""""""""""""""""""""""""""""""""""""""""""""""""
" disable smart tabs
set nosmarttab

set expandtab " use spaces, no tabs

" 1 tab == 4 spaces
set shiftwidth=4
set softtabstop=4

set ai " Auto indent
set si " Smart indent

" modern backspace behavior
set backspace=indent,eol,start

filetype indent on	" enable filetype specific indentation

"""""""""""""""""""""""""""""""""""""""""""""""""
" Movement
"""""""""""""""""""""""""""""""""""""""""""""""""
" move vertically by visual line (don't skip wrapped lines)
nnoremap j gj
nnoremap k gk


"""""""""""""""""""""""""""""""""""""""""""""""""
"  Status line
"""""""""""""""""""""""""""""""""""""""""""""""""

" Set to always show statusline
set laststatus=2

