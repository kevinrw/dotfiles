"""""""""""""""""""""""""""""""""""""""""""""""""
" Custom Keybindings
"""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader="\\"       " leader key (default is '\')

" Show current key bindings
nnoremap <leader>ch :nmap<CR>
" show default key mappings
nnoremap <leader>ha :help index<CR>
nnoremap <leader>hn :nmap<CR>
nnoremap <leader>hv :vmap<CR>
nnoremap <leader>hi :imap<CR>

" reload Vim configuration and rerun custom initialization
nnoremap <silent><leader>i :source ~/.vim/vimrc<CR>:call ReInitialize()<CR>

" searches forward for word under cursor
nnoremap <leader>n *
" searches backward for word under cursor
nnoremap <leader>N #
nnoremap <leader>ff :Files<CR>
nnoremap <leader><C-p> :Files<CR>
nnoremap <leader>fl :Lines<CR>
nnoremap <leader>/ :Lines<CR>
nnoremap <leader>fb :Buffers<CR>
nnoremap <leader><C-b> :Buffers<CR>
nnoremap <leader>fw :Windows<CR>
nnoremap <leader><C-w> :Windows<CR>
nnoremap <leader>fs :Rg<CR>
nnoremap <leader><C-f> :Rg<CR>
nnoremap <leader>fg :GFiles<CR>
nnoremap <leader>fC :Commits<CR>
nnoremap <leader>fc :Commands<CR>
nnoremap <leader>fh :History<CR>
nnoremap <leader><C-r> :History<CR>
nnoremap <leader>f/ :History/<CR>
nnoremap <leader>gw :execute 'vimgrep /' . expand('<cword>') . '/ **/*'<CR>

" quickfix nav
nnoremap <leader>qf :copen<CR>
nnoremap <leader>qc :cclose<CR>
nnoremap <leader>qn :cnext<CR>
nnoremap <leader>qN :cprevious<CR>

nnoremap <leader>qa :confirm quitall<CR>
nnoremap <leader><C-q> :confirm quitall<CR>
nnoremap <leader><C-d> :confirm bdelete<CR>
nnoremap <leader>cd :lcd <C-z>
nnoremap <leader>s :call ToggleScratch()<CR>

" quick buffer navigation
" also can use built-in <C-^> to alternate between buffers
nnoremap <leader>bs :call fzf#run(fzf#wrap('buffers', {'source': BufferList(), 'sink': function('SwitchBuffer'), 'options': '--prompt="SwitchBuffer> "'}))<CR>
nnoremap <leader>bd :call fzf#run(fzf#wrap('buffers', {'source': BufferList(), 'sink': function('DeleteBuffer'), 'options': '--multi --prompt="DeleteBuffer> "'}))<CR>
" works with set wildcharm=<C-z> to send Tab sequence
nnoremap <leader>b<Tab> :buffer <C-z>
" delete buffer
nnoremap <leader>x :confirm bdelete<CR>
" open terminal window
nnoremap <leader>T :terminal<CR>
" map Esc key to go to terminal normal mode
"tnoremap <Esc> <C-\><C-n>
tnoremap <leader><Esc> <C-w>N

" quick buffer navigation
" NOTE: Tab is also a Control sequence
nmap <silent> <Tab> :bnext<CR>
nmap <silent> <S-Tab> :bprev<CR>

" quick split window navigation
nmap <silent> <C-h> <C-w>h
nmap <silent> <C-l> <C-w>l
nmap <silent> <C-j> <C-w>j
nmap <silent> <C-k> <C-w>k

" toggle Explore - override default - same as "j"
nnoremap <C-n> :Lexplore<CR>
nnoremap <leader>e :Lexplore<CR>
" open files - override default - same as "k"
nnoremap <C-p> :Files<CR>
" clear highlights
nnoremap <Esc><Esc> :noh<CR>

