""""""""""""""""""""""""""""""""""""""""""""""""
" Automatic configuration
""""""""""""""""""""""""""""""""""""""""""""""""

" turn off hlsearch when done searching
"augroup vimrc-incsearch-highlight
"    autocmd!
"    autocmd CmdlineEnter /,\? :set hlsearch
"    autocmd CmdlineLeave /,\? :set nohlsearch
"augroup END

augroup vim_start | au!
    au VimEnter * call Initialize()
    "au BufEnter * lcd %:p:h " works best with set noautochdir
augroup end
