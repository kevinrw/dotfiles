function! Initialize() abort
    " auto set base16 theme based on shell
    if !empty(expand('$BASE16_THEME')) && !has('gui_running')
        let g:current_base16_theme = 'base16-' . expand('$BASE16_THEME')
        try
            execute('colorscheme ' . g:current_base16_theme)
        catch
            echo "failed to load colorscheme " . g:current_base16_theme
        endtry
    else
        colorscheme slate
    endif
    call ConfigureSpellCheck()
    " call this last to allow local overrides
    call InitLocalVimRc()
endfunction

" loads local vimrc specific to this machine
function! InitLocalVimRc() abort
    let l:local_vimrc_path = expand('$HOME') . '/.local/vim/vimrc'
    if filereadable(l:local_vimrc_path)
        execute('source ' . l:local_vimrc_path)
    endif
endfunction

" re-initialize vim configuration and plugins
" to be combined with source ~/.vimrc because it cannot be sourced from inside
" itself :p
function! ReInitialize() abort
    execute(':PlugClean')
    execute(':PlugInstall')
    call Initialize()
endfunction

function! BufferList() abort
    let l:blist = getbufinfo({'bufloaded': 1, 'buflisted': 1})
    let l:result = []
    for l:item in l:blist
        " skip unnamed buffers; also skip hidden buffers?
        if empty(l:item.name) || l:item.hidden
            continue
        endif
        call add(l:result, l:item.name)
    endfor
    return l:result
endfunction

function! SwitchBuffer(item) abort
    execute('sbuffer ' . bufnr(a:item))
endfunction

function! DeleteBuffer(item) abort
    execute('confirm bdelete ' . bufnr(a:item))
endfunction

function! ToggleScratch() abort
    let l:blist = getbufinfo({'bufloaded': 1, 'buflisted': 1})
    let l:result = []
    for l:item in l:blist
        if l:item.name =~ "\/Scratch$"
            call add(l:result, l:item)
        endif
    endfor
    if len(l:result) > 0
        let l:scratch = get(l:result, 0, {})
        if l:scratch.hidden
            pedit Scratch
            silent! wincmd P
        else
            pclose
        endif
    else
        pedit Scratch
        silent! wincmd P
    endif
endfunction

function! ConfigureSpellCheck() abort
    highlight clear SpellBad
    highlight clear SpellCap
    highlight SpellBad cterm=underline
    highlight SpellCap cterm=underline
    highlight SpellBad gui=underline
    highlight SpellCap gui=underline
endfunction
