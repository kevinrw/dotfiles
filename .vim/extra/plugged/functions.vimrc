function! Cond(cond, ...)
  let opts = get(a:000, 0, {})
  return a:cond ? opts : extend(opts, { 'on': [], 'for': [] })
endfunction " - avoid using standard vim directory names like 'plugin'

" loads local plugins specific to this machine
function! InitLocalPlugged() abort
    let l:local_pluggedrc_path = expand('$HOME') . '/.local/vim/extra/plugged/init.vimrc'
    if filereadable(l:local_pluggedrc_path)
        execute('source ' . l:local_pluggedrc_path)
    endif
endfunction
