"""""""""""""""""""""""""""""""""""""""""""""""""
" Plugged Functions
"""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/extra/plugged/functions.vimrc

"""""""""""""""""""""""""""""""""""""""""""""""""
" Plugged Main
"""""""""""""""""""""""""""""""""""""""""""""""""
"
" vim-plugin
"
" specify directory for vim-plugin plugins
"
call plug#begin('~/.vim/plugged')

" conditioned examples
"Plug 'benekastah/neomake', Cond(has('nvim'))
" With other options
"Plug 'benekastah/neomake', Cond(has('nvim'), { 'on': 'Neomake' })
"
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'tinted-theming/base16-vim', Cond(!has('gui_running'))
Plug 'christoomey/vim-tmux-navigator'

" Unmanaged plugin (manually installed and updated)
"Plug '~/projects/mwd.vim'
Plug 'k3vinw/mwd.vim'

" initialize plugins local to this machine
call InitLocalPlugged()

" Use rg for FZF
if executable('rg')
  let g:rg_derive_root='true'
endif
" open in new tab by default
" NOTE: this appears to kill handy Ctrl-T/X/V default keybindings for fzf :/
"let g:fzf_action = { 'enter': 'tab split' }

" initialize plugin system
call plug#end()
