# colorize all the things
alias vi='vim'
alias nv='nvim'
alias edit='vim'
alias diff='diff --color=auto'
alias diff-words='git diff --no-index --word-diff=color'
alias grep='grep --color=auto'
alias ip='ip --color=auto'
#alias ls='ls --color=auto -F'
alias ls='exa'
# for storing dotfiles in a git bare repo in a side directory
# https://www.atlassian.com/git/tutorials/dotfiles
#alias dotfiles='git --git-dir=$HOME/.dotfiles/dotfiles.git --work-tree=$HOME'

alias arch-update-aur='paru -Sua'
alias arch-update-sys='pacman -Syu'
alias arch-update-all='paru -Syu'

# open file or url in default browser/app
alias o='xdg-open'

alias pbcopy='wl-copy'
alias pbpaste='wl-paste'
