#
# ~/.bashrc
#

# set custom bash history to avoid conflicts with default bash shells (e.g. bash --norc)
HISTFILE="$HOME/.bash_history_omb"

# enable history append
shopt -s histappend

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# set input mode to vi
set -o vi

# load bash functions
if [ -f "$HOME/.bash_functions" ]; then
	source "$HOME/.bash_functions"
fi

# load bash aliases
if [ -f "$HOME/.bash_aliases" ]; then
	source "$HOME/.bash_aliases"
fi

# push home bin to PATH
if [ -d "$HOME/bin" ]; then
	PATH="${HOME}/bin:${PATH}"
fi

# push $HOME/.local/bin to PATH
# for user installed binaries
if [ -d "$HOME/.local/bin" ]; then
	PATH="${HOME}/.local/bin:${PATH}"
fi

stty -ixon # disable Ctrl-S screen-locking behavior (because seeing is believing)
export IGNOREEOF=1 # ignore first ctrl-D (because accidents happen)
export EDITOR=vim
export BROWSER=w3m

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH=$BUN_INSTALL/bin:$PATH

# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
	    source "$BASE16_SHELL/profile_helper.sh"

# FzF
export FZF_DEFAULT_OPTS="--height=40% --layout=reverse --info=inline --border --margin=0 --padding=0"

function fzf_base16() {
    local pick="$(find ~/.config/base16-shell/scripts/ -type f |xargs -If basename -s .sh f \
        |sed 's/base16-//g' |sort |fzf --prompt="base16-theme> " --sync --bind "enter:execute(printf '%s' {})+abort" \
        |xargs -I{} echo ~/.config/base16-shell/scripts/base16-{}.sh {})"
    if [[ -n "$pick" ]]; then
        _base16 $pick
    else
        return
    fi
}

function fzf_jobs() {
    local COUNT=$(jobs |wc -l)
    [[ "$COUNT" -lt 1 ]] && return
    if [[ "$COUNT" -gt 1 ]]; then
        fg "$(jobs |fzf --prompt "job> " --sync --bind "enter:execute(printf '%s' {} |sed '/\[\([0-9]\{1,\}\)\].*/ s//\1/g')+abort")"
    else
        fg
    fi
}

# Key bindings

# e.g., bind '"\C-x\C-r": re-read-init-file'.
bind -x '"\\i":"source ~/.bashrc"'
bind -x '"\\j":"fzf_jobs"'

#bind -x '"C-x\C-t":"fzf_base16"'
bind -x '"\\t":"fzf_base16"'

# Starship prompt - moved to .bashrc because it stopped working from login shell (.bash_profile) ... not sure why :/
eval "$(starship init bash)"
