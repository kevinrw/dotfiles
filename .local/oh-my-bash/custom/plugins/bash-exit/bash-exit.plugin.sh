#! bash oh-my-bash.module


# default configuration
if [[ ! ${BASHEXIT_FILE-} ]]
then
    BASHEXIT_FILE="${HOME}/.bash_exit"
fi

function _bashexit_exec {
    if [[ -f "${BASHEXIT_FILE}" ]]
    then
        source "${BASHEXIT_FILE}"
    fi
}

trap _bashexit_exec EXIT
