" this file is for plugins local to this machine
" lines below are intented for this section: plug#begin('~/.vim/plugged')
"
"Plug 'itchyny/lightline.vim'
Plug 'daviesjamie/vim-base16-lightline', Cond(!has('gui_running'))
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'

" base16-lightline plugin
let g:lightline = {
\   'colorscheme': 'base16'
\}

" configure airline
let g:airline#extensions#tabline#enabled = 0
let g:airline_powerline_fonts = 1

" lines above are intented for this section: plug#begin('~/.vim/plugged')
