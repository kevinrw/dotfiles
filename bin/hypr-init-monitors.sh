#!/bin/bash
LAPTOP_DISPLAY=eDP-1

function find_monitor() {
    hyprctl -j monitors |jq -r '.[].name' |grep -q "$1"
}

if grep -q open /proc/acpi/button/lid/LID/state
then
    find_monitor "$LAPTOP_DISPLAY" &&
        hypr-clamshell.sh open
else
    !find_monitor "$LAPTOP_DISPLAY" &&
        hypr-clamshell.sh close
fi
