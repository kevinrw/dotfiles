#!/bin/bash
# hyprland multi-monitor dispatcher

MONITORS=$(hyprctl -j monitors |jq -r '.[].name')

for monitor in $MONITORS
do
    hyprctl dispatch $@ $monitor
done

exit 0
