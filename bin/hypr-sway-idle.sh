#!/bin/bash
# hypr sway-idle
lock=$HOME/bin/hypr-lock.sh
suspend=$HOME/bin/suspend.sh
lock_time=300
blank_time=600
# TODO: only suspend when on battery
pgrep swayidle || swayidle -w \
    timeout "$lock_time" "$HOME/bin/hyprctl-monitors.sh dpms off" \
    resume "$HOME/bin/hyprctl-monitors.sh dpms on" \
    timeout "$blank_time" "$suspend" \
    before-sleep "$lock" &

exit 0
