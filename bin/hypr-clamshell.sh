#!/bin/bash
# hyprland clamshell switcher

if [[ "$1" == "open" ]]; then
    hyprctl keyword monitor "eDP-1,1920x1080,2560x0,1"
    hyprctl dispatch dpms on eDP-1
else
    hyprctl keyword monitor "eDP-1,disable"
    hyprctl dispatch dpms off eDP-1
fi

exit 0
