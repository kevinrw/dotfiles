#!/bin/sh
POWER_OFF_SCREENS=0
SUSPEND_SYSTEM=0

# parse sub-command
case "$1" in
  power-off-screens)
    POWER_OFF_SCREENS=1;;
  suspend-system)
    SUSPEND_SYSTEM=1;;
  *);;
esac

function lock() {
	#swaylock -f -c 000000
	swaylock \
		$([[ "$POWER_OFF_SCREENS" == 1 ]] || echo --daemonize) \
                --show-failed-attempts \
		--screenshots \
		--clock \
		--indicator \
		--indicator-radius 100 \
		--indicator-thickness 7 \
		--effect-blur 7x8 \
		--effect-vignette 0.5:0.5 \
		--ring-color bb00cc \
		--key-hl-color 880033 \
		--line-color 00000000 \
		--inside-color 00000088 \
		--separator-color 00000000 \
		--grace 0 \
		--fade-in 0.2
}

if [[ "$POWER_OFF_SCREENS" == 1 ]]; then
	# Times the screen off and puts it to background
	(swayidle \
    timeout 3 "$HOME/bin/hyprctl-monitors.sh dpms off" resume "$HOME/bin/hyprctl-monitors.sh dpms on")&
  SWAYIDLE_PID=$!
fi

# Locks the screen immediately and optionally suspends system
if [[ "$SUSPEND_SYSTEM" == 1 ]]
then
    # no need to call lock here (swayidle will auto lock on suspend)
  if ! pgrep swayidle
  then
    lock
  fi
  systemctl suspend
else
  lock
fi

# Kills swayidle background task so idle timer doesn't keep running
[[ -n "$SWAYIDLE_PID" ]] && kill $SWAYIDLE_PID

exit 0
